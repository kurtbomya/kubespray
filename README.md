# kubespray

### Summary
- Leverage kubespray, ansible based prod ready k8s configurer, to create cluster in GCP

### Context
- https://github.com/kubernetes-sigs/kubespray

### Directories
- `terraform` to create the servers in GCP
- `ansible` to contain the ansible based components

# Components

### Application(s)
- coredns
- cert-manager
- argocd
### Core Component(s)
- Kubernetes
- containerd
- etcd
### Linux Distribution
- Ubuntu 20.04
### Network Plugin
- cilium

### Usage

#### Installing Ansible
1. Create virtualenv, activate the virtualenv and install requirements:
1. Edit `install-kubespray-ansible.sh` and add the version of ansible you'd like to use to the provided variable.
1. Run `./install-kubespray-ansible.sh`!

#### Bootstrapping Terraform
1. Move to the terraform directory from kubespray project `cd kubespray/contrib/terraform/gcp`
1. Generate credentials file from Google Cloud Platform, IAM, Service Accounts, json extract, and place into `kubespray/contrib/terraform/gcp/service-account.json`
1. Populate `kubespray/contrib/terraform/gcp/tfvars.json` file with required vars. For example:

```json
{
  "gcp_project_id": "kubespray-away",
  "region": "us-central1",
  "ssh_pub_key": "~/.ssh/id_rsa.pub",

  "keyfile_location": "service-account.json",

  "prefix": "development",

  "ssh_whitelist": [
    "<YOURIP>/32"
  ],
  "api_server_whitelist": [
    "<YOURIP>/32"
  ],
  "nodeport_whitelist": [
    "<YOURIP>/32"
  ],
  "ingress_whitelist": [
    "0.0.0.0/0"
  ],

  "machines": {
    "primary-0": {
      "node_type": "master",
      "size": "n1-standard-2",
      "zone": "us-central1-a",
      "additional_disks": {},
      "boot_disk": {
        "image_name": "ubuntu-os-cloud/ubuntu-2004-focal-v20220118",
        "size": 50
      }
    },
    "worker-0": {
      "node_type": "worker",
      "size": "n1-standard-8",
      "zone": "us-central1-a",
      "additional_disks": {
        "extra-disk-1": {
          "size": 100
        }
      },
      "boot_disk": {
        "image_name": "ubuntu-os-cloud/ubuntu-2004-focal-v20220118",
        "size": 50
      }
    },
    "worker-1": {
      "node_type": "worker",
      "size": "n1-standard-8",
      "zone": "us-central1-a",
      "additional_disks": {
        "extra-disk-1": {
          "size": 100
        }
      },
      "boot_disk": {
        "image_name": "ubuntu-os-cloud/ubuntu-2004-focal-v20220118",
        "size": 50
      }
    }
  }
}
```

#### Terraform execution

1. `terraform init`
1. `terraform plan -var-file="tfvars.json"`
1. Activate API(s) for GCP:
    - compute
1. `terraform apply -var-file="tfvars.json"`

### Outputs we need for later step

```tf
control_plane_lb_ip_address = "<REDACTED>"
ingress_controller_lb_ip_address = "<REDACTED>"
master_ips = {
  "development-primary-0" = {
    "private_ip" = "10.0.10.4"
    "public_ip" = "<REDACTED>"
  }
}
worker_ips = {
  "development-worker-0" = {
    "private_ip" = "10.0.10.2"
    "public_ip" = "<REDACTED>"
  }
  "development-worker-1" = {
    "private_ip" = "10.0.10.3"
    "public_ip" = "<REDACTED>"
  }
}
```

#### Prepare inventory.py environment

We need to generate the inventory file. Kubespray has a python file that does this for us, but it requires libraries not already installed.

1. Create a virtualenv for inventory.py
2. Install requirements for this file `pip install -r kubespray/contrib/inventory_builder/requirements.txt`
3. Move to the directory `cd kubespray/contrib/inventory_builder/`
4. `declare -a IPS=(<REDACTED> <REDACTED> <REDACTED>)`, space delimited
5. Move to the directory kubespray (the one we've the gitmodule active for)
6. `CONFIG_FILE=inventory/mycluster/hosts.yaml python contrib/inventory_builder/inventory.py ${IPS[@]}`
7. Configure `hosts.yaml` to have the `ansible_host` contain public IP and `ip` & `access_ip` to contain the local 10.x ips.


#### Execute installation
```bash
ansible-playbook -i inventory/mycluster/hosts.yaml --become --become-user=root cluster.yml --key-file "~/.ssh/id_rsa" --user=ubuntu --extra-vars kubeconfig_localhost=true
```
